package com.devcamp.thigiuaky.restapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.thigiuaky.restapi.model.Department;

public interface departmentRepository extends JpaRepository<Department, Integer> {
    
}