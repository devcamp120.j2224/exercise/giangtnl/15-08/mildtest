package com.devcamp.thigiuaky.restapi.model;

import java.util.*;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;


@Entity
@Table(name = "department_table")
public class Department {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true)
    private int id;

    @Column(name = "ma_phong_ban")
    @NotEmpty(message = "Không được để trống Mã phòng ban")
    private String maPhongBan;

    

    @Column(name = "ten_phong_ban")
    @NotEmpty(message = "Không được để trống Tên phòng ban")
    private String tenPhongBan;

    @Column(name = "nghiep_vu")
    private String nghiepVu;

    @Column(name = "gioi_thieu")
    private String gioiThieu;

    @OneToMany(targetEntity = Staff.class, cascade = CascadeType.ALL)

    @JoinColumn(name = "department_id")
    private List<Staff> staffs;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMaPhongBan() {
        return maPhongBan;
    }

    public void setMaPhongBan(String maPhongBan) {
        this.maPhongBan = maPhongBan;
    }

    public String getTenPhongBan() {
        return tenPhongBan;
    }

    public void setTenPhongBan(String tenPhongBan) {
        this.tenPhongBan = tenPhongBan;
    }

    public String getNghiepVu() {
        return nghiepVu;
    }

    public void setNghiepVu(String nghiepVu) {
        this.nghiepVu = nghiepVu;
    }

    public String getGioiThieu() {
        return gioiThieu;
    }

    public void setGioiThieu(String gioiThieu) {
        this.gioiThieu = gioiThieu;
    }

    public List<Staff> getStaffs() {
        return staffs;
    }

    public void setStaffs(List<Staff> staffs) {
        this.staffs = staffs;
    }

    public Department() {
    }

    public Department(int id, String maPhongBan, String tenPhongBan, String nghiepVu, String gioiThieu, List<Staff> staffs) {
        this.id = id;
        this.maPhongBan = maPhongBan;
        this.tenPhongBan = tenPhongBan;
        this.nghiepVu = nghiepVu;
        this.gioiThieu = gioiThieu;
        this.staffs = staffs;
    }

}