package com.devcamp.thigiuaky.restapi.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.thigiuaky.restapi.model.Department;
import com.devcamp.thigiuaky.restapi.model.Staff;
import com.devcamp.thigiuaky.restapi.repository.departmentRepository;
import com.devcamp.thigiuaky.restapi.repository.staffRepository;


@RequestMapping("/")
@CrossOrigin
@RestController
public class ApiController {
    @Autowired
    staffRepository staffRepository;
    @Autowired
    departmentRepository departmentRepository;
    // api lấy danh sách toàn bộ department
    @GetMapping("/departments")
    public ResponseEntity<List<Department>> getAllDepartment() {
        try {
            List<Department> departments = new ArrayList<Department>();
            departmentRepository.findAll().forEach(departments::add);
            return new ResponseEntity<>(departments, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
    // api lấy danh sách toàn bộ staff
    @GetMapping("/staffs")
    public ResponseEntity<List<Staff>> getAllStaff() {
        try {
            List<Staff> staffs = new ArrayList<Staff>();
            staffRepository.findAll().forEach(staffs::add);
            return new ResponseEntity<>(staffs, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
    // lấy thông tin department bởi departmentId
    @GetMapping("/departments/{departmentid}")
    public ResponseEntity<Department> getAllDepartmentById(@PathVariable("departmentid") int departmentid) {
        Optional<Department> department = departmentRepository.findById(departmentid);
        if (department.isPresent()) {
            return new ResponseEntity<>(department.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    // lấy thông tin STAFF bởi staffId
    @GetMapping("/staffs/{staffid}")
    public ResponseEntity<Staff> getAllImageById(@PathVariable("staffid") int staffid) {
        Optional<Staff> staff = staffRepository.findById(staffid);
        if (staff.isPresent()) {
            return new ResponseEntity<>(staff.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    // Tạo department
    @PostMapping("/department/create")
    public ResponseEntity<Object> createDepartment(@Valid @RequestBody Department department) {
        Optional<Department> departmentSearch = departmentRepository.findById(department.getId());
        try {

            if (departmentSearch.isPresent()) {
                return ResponseEntity.unprocessableEntity().body(" department already exsit ");
            }
            department.setMaPhongBan(department.getMaPhongBan());
            department.setTenPhongBan(department.getTenPhongBan());
            department.setNghiepVu(department.getNghiepVu());
            department.setGioiThieu(department.getGioiThieu());
            department.setStaffs(department.getStaffs());
            Department departmentCreate = departmentRepository.save(department);
            return new ResponseEntity<>(departmentCreate, HttpStatus.CREATED);
        } catch (Exception e) {

            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified department: " +
                            e.getCause().getCause().getMessage());
        }

    }
    //tạo nhân viên cho phòng ban
    @PostMapping("/department/{departmentid}/staff/create")
    public ResponseEntity<Object> createStaff(@PathVariable("departmentid") int departmentid, @Valid @RequestBody Staff staff) {
        Optional<Department> departmentSearch = departmentRepository.findById(departmentid);
        Optional<Staff> staffCreateId = staffRepository.findById(staff.getId());
        if (departmentSearch.isPresent()) {
            try {

                if (staffCreateId.isPresent()) {
                    return ResponseEntity.unprocessableEntity().body(" staff already exsit ");
                }
                Department department = departmentSearch.get();
                staff.setMaNhanVien(staff.getMaNhanVien());
                staff.setTenNhanVien(staff.getTenNhanVien());
                staff.setChucVu(staff.getChucVu());
                staff.setGioiTinh(staff.getGioiTinh());
                staff.setNgaySinh(staff.getNgaySinh());
                staff.setDiaChi(staff.getDiaChi());
                staff.setSoDienThoai(staff.getSoDienThoai());
                staff.setDepartment(department);

                Staff staffCreate = staffRepository.save(staff);
                return new ResponseEntity<>(staffCreate, HttpStatus.CREATED);
            } catch (Exception e) {

                return ResponseEntity.unprocessableEntity()
                        .body("Failed to Create specified staff: " +
                                e.getCause().getCause().getMessage());
            }
        } else {
            return null;
        }

    }
    //cập nhật thông tin phòng ban
    @PutMapping("/departments/update/{departmentid}") // Dùng phương thức PUT
    public ResponseEntity<Object> updateDepartmentById(@PathVariable("departmentid") int departmentid,
    @Valid @RequestBody Department department) {
        Optional<Department> departmentId = departmentRepository.findById(departmentid);
        try{
        if (departmentId.isPresent()) {
            Department departmentEdit = departmentId.get();
            departmentEdit.setMaPhongBan(department.getMaPhongBan());
            departmentEdit.setTenPhongBan(department.getTenPhongBan());
            departmentEdit.setNghiepVu(department.getNghiepVu());
            departmentEdit.setGioiThieu(department.getGioiThieu());
            departmentEdit.setStaffs(department.getStaffs());
            Department departmentUpdate = departmentRepository.save(departmentEdit);
            return new ResponseEntity<>(departmentUpdate, HttpStatus.OK);
        }else {
            return ResponseEntity.badRequest().body("Failed to get specified Department: " + departmentid + "  for update.");
        }    
        } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Failed to Update specified Department:" + e.getCause().getCause().getMessage());
            }
    }
    //cập nhật thông tin nhân viên
    @PutMapping("/staffs/update/{staffid}") // Dùng phương thức PUT
    public ResponseEntity<Object> updateStaffById(@PathVariable("staffid") int staffid,
    @Valid @RequestBody Staff staff) {
        Optional<Staff> staffId = staffRepository.findById(staffid);
        try {
        if (staffId.isPresent()) {
            Staff staffEdit = staffId.get();
            staffEdit.setMaNhanVien(staff.getMaNhanVien());
            staffEdit.setTenNhanVien(staff.getTenNhanVien());
            staffEdit.setChucVu(staff.getChucVu());
            staffEdit.setGioiTinh(staff.getGioiTinh());
            staffEdit.setNgaySinh(staff.getNgaySinh());
            staffEdit.setDiaChi(staff.getDiaChi());
            staffEdit.setSoDienThoai(staff.getSoDienThoai());
            Staff staffUpdate = staffRepository.save(staffEdit);
            return new ResponseEntity<>(staffUpdate, HttpStatus.OK);
        } else {
            return ResponseEntity.badRequest().body("Failed to get specified Staff: " + staffid + "  for update.");
        }    
        } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Failed to Update specified Staff:" + e.getCause().getCause().getMessage());
        }    
    }
    //xóa thông tin phòng ban
    @DeleteMapping("/departments/delete/{departmentid}") // Dùng phương thức DELETE
    public ResponseEntity<Department> deleteDepartmentById(@PathVariable("departmentid") int departmentid) {
        try {
            departmentRepository.deleteById(departmentid);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    // xóa thông tin nhân viên
    @DeleteMapping("/staffs/delete/{staffid}") // Dùng phương thức DELETE
    public ResponseEntity<Staff> deleteStaffById(@PathVariable("staffid") int staffid) {
        try {
            staffRepository.deleteById(staffid);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    // xóa toàn bộ phòng ban
    @DeleteMapping("/departments/delete")// Dùng phương thức DELETE
	public ResponseEntity<Department> deleteAllDepartment() {
		try {
			departmentRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
    //xóa toàn bộ nhân viên
    @DeleteMapping("/staffs/delete")// Dùng phương thức DELETE
	public ResponseEntity<Staff> deleteAllStaff() {
		try {
			staffRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}