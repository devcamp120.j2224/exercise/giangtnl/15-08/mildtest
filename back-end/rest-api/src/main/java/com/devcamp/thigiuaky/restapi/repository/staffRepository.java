package com.devcamp.thigiuaky.restapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.thigiuaky.restapi.model.Staff;

public interface staffRepository extends JpaRepository<Staff, Integer> {
    
}