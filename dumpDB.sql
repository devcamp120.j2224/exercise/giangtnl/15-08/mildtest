INSERT INTO `department_table`(`id`, `gioi_thieu`, `ma_phong_ban`, `nghiep_vu`, `ten_phong_ban`) VALUES 
  (1, 'TP.HCM','D01', 'Kỹ thuật','Phòng Công nghệ Thông tin'),
  (2, 'Long An', 'D02', 'Ngoại ngữ', 'Phòng ngoại giao'),
  (3, 'Tây Ninh', 'D03', 'Văn học', 'Ban biên tập'),
  (4, 'Đồng Tháp', 'D04', 'Tài chính', 'Phòng Kế toán - Tài chính'),
  (5, 'An Giang', 'D05', 'Quản Trị', 'Ban Giám đốc'),
  (6, 'Hậu Giang', 'D06', 'Vệ sinh', 'Bộ phận vệ sinh'),
  (7, 'Kiên Giang', 'D07', 'Nhân sự', 'Phòng Tổ chức cán bộ'),
  (8, 'Cần Thơ', 'D08','Hành chính', 'Phòng Hành chính'),
  (9, 'Vĩnh Long', 'D09', 'Pháp chế', 'Phòng Pháp chế'),
  (10, 'Tiền Giang', 'D10', 'Kinh doanh', 'Phòng Kinh doanh');

INSERT INTO `staff_table`(`id`, `chuc_vu`, `dia_chi`, `gioi_tinh`, `ma_nhan_vien`, `ngay_sinh`, `so_dien_thoai_lien_he`, `ten_nhan_vien`, `department_id`) VALUES 
('1','Trưởng phòng','Quận 1','Nữ', 'S01', '12/07/1994','0909090909','Linh Giang','1'),
('2','Phó phòng','Quận 2','Nữ','S02', '12/08/1995','0808080808','Châu Giang','2'),
('3','Team leader','Quận 3', 'Nam', 'S03','12/01/1995','070707070707','Hoàng Nam','3'),
('4','Nhân viên','Quận 4', 'Nam', 'S04', '12/02/1996','06060606060','Hoàng Linh','4'),
('5','Nhân viên','Quận 5','Nam', 'S05', '12/02/1997','050505050505','Hoàng Long','5');

